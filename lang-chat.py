import os
import openai
import sys
from dotenv import load_dotenv, find_dotenv
from langchain.document_loaders import PyPDFLoader
from langchain.document_loaders.generic import GenericLoader
from langchain.document_loaders.parsers import OpenAIWhisperParser
from langchain.document_loaders.blob_loaders.youtube import YoutubeAudioLoader

sys.path.append("../..")
_ = load_dotenv(find_dotenv())  # read local .env file

loader = PyPDFLoader("docs/cs229_lectures/MachineLearnin-Lecture01.pdf")
pages = loader.load()

page = page[0]
print(page.page_content[:500])
print(page.metadata)

url = "https://www.youtube.com/watch?v=jGw0_UgTS7I"
save_dir = "docs/youtube/"